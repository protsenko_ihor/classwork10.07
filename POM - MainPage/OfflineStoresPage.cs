﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpecFlowTests.POM___MainPage
{
    public class OfflineStoresPage
    {
        private IWebDriver _driver;
        public By getOfflineStoresPagePageTitle = By.XPath("/html/head/title");
        public By get_H1_GoOfflineStorePage = By.XPath("/html/body/div[1]/div/div/div[2]/div[1]/div[1]/h1");

        public By getDniproAdressButton = By.XPath("/html/body/div[1]/div/div/div[2]/div[1]/ul/li[1]/a");
        public By getKievAdressButton = By.XPath("/html/body/div[1]/div/div/div[2]/div[1]/ul/li[2]/a");
        public By getOdessaAdressButton = By.XPath("/html/body/div[1]/div/div/div[2]/div[1]/ul/li[3]/a");
        public By getKharkovAdressButton = By.XPath("/html/body/div[1]/div/div/div[2]/div[1]/ul/li[4]/a");

        public By getDniproAdressList = By.XPath("/html/body/div[1]/div/div/div[2]/div[1]/div[3]/div/h2");
        public By getKievAdressList = By.XPath("/html/body/div[1]/div/div/div[2]/div[1]/div[3]/div/h2");
        public By getOdessaAdressList = By.XPath("/html/body/div[1]/div/div/div[2]/div[1]/div[3]/div/h2");
        public By getKharkovAdressList = By.XPath("/html/body/div[1]/div/div/div[2]/div[1]/div[3]/div/h2");


        public OfflineStoresPage(IWebDriver driver)
        {
            this._driver = driver;
        }

        public IWebElement FindTitleOfflineStoresPage()
        {
            return _driver.FindElement(getOfflineStoresPagePageTitle);
        }

        public string GetTextFromTitleOfflineStoresPage()
        {
            return FindTitleOfflineStoresPage().GetAttribute("text");
        }


        public IWebElement Find_H1_OfflineStoresPage()
        {
            return _driver.FindElement(get_H1_GoOfflineStorePage);
        }

        public string GetTextFrom_H1_GoOfflineStorePage()
        {
            return Find_H1_OfflineStoresPage().Text;
        }

        public OfflineStoresPage ClickDniproAdressButton()
        {
            _driver.FindElement(getDniproAdressButton).Click();

            return this;
        }

        public OfflineStoresPage ClickOnKievAdressButton()
        {
            _driver.FindElement(getKievAdressButton).Click();

            return this;
        }

        public OfflineStoresPage ClickOnOdessaAdressButton()
        {
            _driver.FindElement(getOdessaAdressButton).Click();

            return this;
        }

        public OfflineStoresPage ClickOnKharkovAdressButton()
        {
            _driver.FindElement(getKharkovAdressButton).Click();

            return this;
        }


        public string GetTextFromDniproAdressList()
        {
            return _driver.FindElement(getDniproAdressList).Text;
        }

        public string GetTextFromKievAdressList()
        {
            return _driver.FindElement(getKievAdressList).Text;
            
        }

        public string GetTextFromOdessaAdressList()
        {
            return _driver.FindElement(getOdessaAdressList).Text;
        }

        public string GetTextFromKharkovAdressList()
        {
            return _driver.FindElement(getKharkovAdressList).Text;
        }

    }
}
