﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpecFlowTests.POM___Header
{
    class WarrantyAndRefundPage
    {
        private IWebDriver _driver;
        public By getWarrantyAndRefundPageTitle = By.XPath("/html/head/title");



        public WarrantyAndRefundPage(IWebDriver driver)
        {
            this._driver = driver;
        }

        public IWebElement FindTitleContactsPage()
        {
            return _driver.FindElement(getWarrantyAndRefundPageTitle);
        }

        public string GetTextFromTitleWarrantyAndRefundPage()
        {
            return FindTitleContactsPage().GetAttribute("text");
        }






    }
}
