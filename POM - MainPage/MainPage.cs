﻿using OpenQA.Selenium;
using SpecFlowTests.POM___Header;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpecFlowTests.POM___MainPage
{
    class MainPage
    {
        private IWebDriver _driver;

        public By getMainPagePageTitle = By.XPath("/html/head/title/text()");

        public By ButtonDarkTheme = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[2]/span[1]");
        public By ToggleThemeSwitcher = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[2]/div");
        public By ButtonLightTheme = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[2]/span[2]");
        public By ButtonBlog = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[1]/a");
        public By ButtonFishka = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[2]/a");
        public By ButtonJobPositions = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[3]/a");
        public By ButtonShops = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[4]/a");
        public By ButtonDeliveryAndPay = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[5]/a");
        public By ButtonCredit = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[6]/a");
        public By ButtonWarrantyAndRefund = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[7]/a");
        public By ButtonContacts = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[8]/a");
        public By ButtonRu = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[4]/span[1]");
        public By ButtonUa = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[4]/span[3]");
        public By ToggleLanguageSwitcher = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[4]/span[2]/div");

        public By mainLabel = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[2]/div/a/img[1]");
        public By linkLeaveSmart = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[2]/div/ul/li[1]/a");
        public By linkAlloMoney = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[2]/div/ul/li[2]/a");
        public By linkAlloUpgrade = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[2]/div/ul/li[3]/a");
        public By linkAlloExchange = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[2]/div/ul/li[4]/a");
        public By linkCutInPrice = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[2]/div/ul/li[5]/a");
        public By buttonContacts = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/ul/li[2]/div/div[1]");
        public By inputSearch = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/ul/li[4]/div/div/form/label/input");
        public By buttonLogin = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/ul/li[5]/div");
        public By buttonCart = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/ul/li[6]/div/div[1]");

        public MainPage(IWebDriver driver)
        {
            this._driver = driver;
        }

        public IWebElement FindTitleMainPage()
        {
            return _driver.FindElement(getMainPagePageTitle);
        }

        public string GetTextFromTitleMainPage()
        {
            return FindTitleMainPage().GetAttribute("text");
        }

        public WarrantyAndRefundPage ClickOnButtonWarrantyAndRefundPage()
        {
            _driver.FindElement(ButtonWarrantyAndRefund).Click();
            return new WarrantyAndRefundPage(_driver);
        }

        public ContactsPage ClickOnContactsPage()
        {
            _driver.FindElement(ButtonContacts).Click();
            return new ContactsPage(_driver);
        }






    }
}
