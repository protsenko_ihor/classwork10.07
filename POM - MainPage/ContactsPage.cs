﻿using OpenQA.Selenium;
using SpecFlowTests.POM___MainPage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpecFlowTests.POM___Header
{
    class ContactsPage
    {
        private IWebDriver _driver;
        public By getContactsPageTitle = By.XPath("/html/head/title/text()");
        public By getButtonGoOfflineStorePage = By.XPath("/html/body/div[1]/div/div/div[2]/div/div/div/div/div/div[1]/div[4]/div/a");

        
        public ContactsPage(IWebDriver driver)
        {
            this._driver = driver;
        }

        public IWebElement FindTitleContactsPage()
        {
            return _driver.FindElement(getContactsPageTitle);
        }

        public string GetTextFromTitleContactsPage()
        {
            return FindTitleContactsPage().GetAttribute("text");
        }

        public OfflineStoresPage ClickOnOfflineStorePagePage()
        {
            _driver.FindElement(getButtonGoOfflineStorePage).Click();
            return new OfflineStoresPage(_driver);
        }

       

    }
}
