﻿Feature: LanguageSwitcher
  As a user
  I want to switch language toggle
  In order to read site information in native language

@mytag
Scenario: Language toggle switched on Russian language
  Given Allo website is opened
  Given User is not authorized 
  And the language of main page is set as Russian
  When User clicks on language toggle
  Then language is set as Ukrainian