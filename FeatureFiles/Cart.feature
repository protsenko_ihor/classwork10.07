﻿Feature: Cart

As a user 
I want to have access to my cart 
In order to see the list of added products


Scenario: An unauthorized user has access to an empty cart
	Given Allo website is open
	Given User is not logged in
	When User clicks on cart
	Then Cart is empty