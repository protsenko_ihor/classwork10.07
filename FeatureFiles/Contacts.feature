﻿Feature: Contacts
 As a user
  I want to see contact information
  In order to see phone number and call to contact center 

  As a user 
  I want to see address list of shops
  In order to see list of shops in chosen city


@mytag
Scenario: Open contacts page
  Given Allo page is opened
  And user is not authorized
  When User clicks on 'Контакты'
  Then Contact page is opened and contact information is shown

Scenario: Open list of shops in chosen city
  Given Allo page is opened
  And user is not authorized
  When User clicks on 'Контакты'
  When User clicks on 'Go_to_map' button
  When User choses city
  Then User sees address list of shops in chosen city